# frozen_string_literal: true

require "json"
require "strum/esb/functions"

module Strum
  module Esb
    # Sneakers helper for Strum
    module Handler
      def self.included(base)
        base.class_eval do
          include Sneakers::Worker
        end
        base.extend ClassMethods
      end

      module ClassMethods
        def bind_to(queue, message_type, message_binding, handler = nil)
          bindings ||= (queue_opts && queue_opts[:bindings]) || {}
          bindings[message_type] ||= []
          bindings[message_type] << message_binding
          from_queue queue, bindings: bindings
          _, *msg = Strum::Esb::Functions.public_send("#{message_type}_explain", message_binding)
          @handlers ||= {}
          params = msg.map{ |param| param&.to_s.gsub(/[^a-zA-Z0-9]/, "_")&.downcase }
          handler_key = ([message_type] + params).join("-")
          @handlers[handler_key] = handler.to_s if handler
        end

        def handlers
          @handlers || {}
        end
      end

      def work_with_params(deserialized_msg, _delivery_info, metadata)
        notice_data = JSON.parse(deserialized_msg)

        snake_case_modify = ->(string) { string.nil? ? nil : string&.to_s.gsub(/[^a-zA-Z0-9]/, "_")&.downcase }
        parse_header = ->(string) { metadata[:headers] && metadata[:headers][string] }
        header = parse_header >> snake_case_modify

        action = header.call("action")
        resource = header.call("resource")
        event = header.call("event")
        state = header.call("state")
        info = header.call("info")
        notice = header.call("notice")
        Thread.current[:chain] = header.call("chain")
        Thread.current[:pipeline] = header.call("pipeline")
        Thread.current[:pipeline_id] = header.call("pipeline-id")

        after_headers_hook

        methods_names = if action
                          action_handler_methods(action, resource)
                        elsif event
                          event_handler_methods(resource, event, state)
                        elsif info
                          info_handler_methods(info)
                        elsif notice
                          notice_handler_methods(resource, notice)
                        end

        method_name = ([*methods_names] << "handler").find { |n| respond_to?(n, include_all: true) }

        unless method_name
          logger.error "Handler not found. Message rejected #{metadata[:headers]} with payload #{notice_data}"
          return reject!
        end

        method_params = method(method_name)
                        .parameters
                        .map { |param| _, param_name = param; param_name }
                        .then { |m| m & %I[action resource event state info chain] }
        handler_params = method_params.each_with_object({}) { |i, res| res[i] = eval(i.to_s); }
        logger.info("Handler #{method_name} found. Payload: #{notice_data}")
        handler_params.count.positive? ? send(method_name, notice_data, handler_params) : send(method_name, notice_data)
        logger.info("Handler #{method_name} executed")
        ack!
      rescue StandardError => e
        logger.error e
        reject!
      end

      def after_headers_hook; end

      private

        def action_handler_methods(action, resource)
          if (custom_handler = self.class.handlers[["action", action, resource].join("-")])
            %W[#{custom_handler} action_#{action}_#{resource} action_handler]
          else
            %W[action_#{action}_#{resource} action_handler]
          end
        end

        def event_handler_methods(resource, event, state)
          result = []
          custom_handler = self.class.handlers[["event", resource, event, state].join("-")]
          result << custom_handler if custom_handler
          result << "event_#{resource}_#{event}_#{state}"
          result << "event_#{resource}_#{event}" if state.eql?("success")
          result << "event_handler"
          result
        end

        def info_handler_methods(info)
          if (custom_handler = self.class.handlers[["info", info].join("-")])
            %W[#{custom_handler} info_#{info} info_handler]
          else
            %W[info_#{info} info_handler]
          end
        end

        def notice_handler_methods(resource, notice)
          custom_handler = self.class.handlers[["notice", resource, notice].join("-")]
          result = []
          result << custom_handler if custom_handler
          result << "notice_#{resource}_#{notice}" if resource
          result << "notice_#{notice}"
          result << "notice_handler"
          result
        end
    end
  end
end
