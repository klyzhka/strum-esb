# frozen_string_literal: true

require "strum/esb/message"

module Strum
  module Esb
    # Action message
    class Info < Message
      class << self
        def call(payload, resource, exchange: Strum::Esb.config.info_exchange, **opts)
          publish(headers: { info: resource }, payload: payload, exchange: exchange, **opts)
        end
      end
    end
  end
end
