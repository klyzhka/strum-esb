# frozen_string_literal: true

module Strum
  module Esb
    VERSION = "0.1.2"
  end
end
