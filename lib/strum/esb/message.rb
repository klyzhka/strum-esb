# frozen_string_literal: true

module Strum
  module Esb
    # Strum Message
    class Message
      class << self
        def publish(exchange:, headers:, payload:)
          rabbit_exchange ||= Strum::Esb.config.rabbit_channel_pool.with { |rabbit_channel| rabbit_channel.headers(exchange, durable: true) }
          properties = {
            headers: headers,
            content_type: "application/json"
          }

          Strum::Esb.config.pre_publish_hooks.each { |hook| hook.call(payload, properties) }

          properties[:headers] = {} unless properties[:headers].is_a?(Hash)
          properties[:headers]["pipeline"] ||= Thread.current[:pipeline] if Thread.current[:pipeline]
          properties[:headers]["pipeline-id"] ||= Thread.current[:pipeline_id] if Thread.current[:pipeline_id]
          if (chain = Thread.current[:chain])
            payload["chain"] ||= chain
            headers["chain"] ||= chain
          end
          
          rabbit_exchange.publish(payload.to_json, **properties)
        end
      end
    end
  end
end
