# frozen_string_literal: true

require_relative "message"

module Strum
  module Esb
    # Action message
    class Action < Message
      class << self
        def call(payload, action, resource, exchange: Strum::Esb.config.action_exchange, **opts)
          publish(headers: { resource: resource, action: action }, payload: payload, exchange: exchange, **opts)
        end
      end
    end
  end
end
