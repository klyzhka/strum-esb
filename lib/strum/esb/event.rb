# frozen_string_literal: true

require_relative "message"

module Strum
  module Esb
    # Event message
    class Event < Message
      class << self
        %i[success failure].each do |method_name|
          define_method method_name do |payload, resource, event, exchange: Strum::Esb.config.event_exchange, **opts|
            publish(headers: {
                      resource: resource,
                      event: event,
                      state: method_name
                    },
                    payload: payload,
                    exchange: exchange,
                    **opts)
          end
        end
      end
    end
  end
end
