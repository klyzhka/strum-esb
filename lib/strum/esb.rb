# frozen_string_literal: true

require "strum/esb/version"

require "json"
require "bunny"
require "sneakers"
require "connection_pool"
require "etc"
require "strum/patch/sneakers/queue_patch"
require "strum/esb/handler"
require "strum/esb/message"
require "strum/esb/action"
require "strum/esb/event"
require "strum/esb/notice"
require "strum/esb/info"
require "dry/configurable"

module Strum
  module Esb
    extend Dry::Configurable

    setting :sneakers_workers, ENV.fetch("SNEAKERS_WORKERS", 1)
    setting :exchange, "strum.general"
    setting :info_exchange, "strum.info"
    setting :event_exchange, "strum.events"
    setting :action_exchange, "strum.actions"
    setting :notice_exchange, "strum.notice"
    setting :rabbit_channel_pool, begin
                                    ConnectionPool.new(size: 5, timeout: 5) do
                                      rabbit_connection = Bunny.new
                                      rabbit_connection.start
                                      rabbit_connection.create_channel
                                    end
                                  end
    setting :before_fork_hooks, []
    setting :after_fork_hooks, []
    setting :pre_publish_hooks, []

    Strum::Esb.config.before_fork_hooks << proc { DB.disconnect } if defined?(DB)

    Strum::Esb.config.after_fork_hooks << proc do
    end

    class Error < StandardError; end

    Sneakers.configure(
      log: $stdout,
      workers: Strum::Esb.config.sneakers_workers,
      hooks: {
        before_fork: -> { Strum::Esb.config.before_fork_hooks.each(&:call) },
        after_fork: -> { Strum::Esb.config.after_fork_hooks.each(&:call) }
      },
      exchange: Strum::Esb.config.exchange,
      exchange_type: "headers"
    )
    Sneakers.logger.level = Logger::INFO
  end
end
