
# Changelog
All notable changes to this project will be documented in this file.

## [0.1.1] - 2021-06-01
### Added
- `pre_publish_hooks` to configuration by [@Samuron]

### Fixed
- bindings for `notice` by [@anton.klyzhka].
- custom handlers for resource name with `-` by [@anton.klyzhka].

### Changed
- return `chain` to header and payload for back compatibility by [@anton.klyzhka].
