# Strum::Esb

Useful sender RabbitMQ messages and handle them

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'strum-esb'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install strum-esb

## Usage

### Configuration

Exchanges can be customized by each message type:
```ruby
Strum::Esb.config.info_exchange = `demo.info`
Strum::Esb.config.event_exchange = `demo.events`
Strum::Esb.config.action_exchange = `demo.actions`
Strum::Esb.config.notice_exchange = `demo.notice`
```

Pre publish hooks allow you to configure custom code that will be able to modify properties of the message that will be published or execute any arbitrary logic
```ruby
Strum::Esb.config.pre_publish_hooks << ->(body, properties) { properties[:correlation_id] = "hello world" }
```

### Sending message:

```ruby
Strum::Esb::Action.call(`payload`, `action`, `resource`)
```

```ruby
Strum::Esb::Event.success(`payload`, `resource`, `*events`)
Strum::Esb::Event.failure(`payload`, `resource`, `*events`)
```

```ruby
Strum::Esb::Info.call(`payload`, `resource`)
```

```ruby
Strum::Esb::Notice.call(`payload`, `notice`)
Strum::Esb::Notice.call(`payload`, `notice`, `resource`)
```

### Handling message:

```ruby
require "strum/esb"

module Queues
  class Resource
    include Strum::Esb::Handler

    from_queue "resource-queue",
               bindings: {
                 events: %w[user/create user/update],
                 
               }

    def handler(payload)
      # code here ...
    end
  end
end
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://code.qpard.com/srtrum/strum-esb.

